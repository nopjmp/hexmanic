use std::alloc::System;

#[global_allocator]
static A: System = System;

use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::{self, Write, SeekFrom};

mod hexfmt;
use hexfmt::WriteHex;

extern crate getopts;
use getopts::Options;

struct HexWriter<W> {
    inner: W,
    pos: usize,
    colwidth: usize,
    long: bool,
}

impl<W: Write> HexWriter<W> {
    pub fn new(inner: W, offset: usize, colwidth: usize, long: bool) -> HexWriter<W> {
        HexWriter {
            inner,
            pos: offset,
            colwidth,
            long,
        }
    }
}

const SPACE: [u8; 16] = [b' '; 16];
impl<W: Write> Write for HexWriter<W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        if buf.len() == 0 {
            return Ok(0);
        }

        self.inner.write(b"$")?;
        if self.long {
            self.pos.write_hex(&mut self.inner)?;
        } else {
            (self.pos as u32).write_hex(&mut self.inner)?;
        }
        self.inner.write(b"  ")?;

        for (i, &b) in buf.iter().enumerate() {
            if i == self.colwidth / 2 {
                self.inner.write(b" ")?;
            }
            b.write_hex(&mut self.inner)?;
            self.inner.write(b" ")?;
        }

        for n in buf.len()..16 {
            if n == self.colwidth / 2 {
                self.inner.write(b"    ")?;
            } else {
                self.inner.write(b"   ")?;
            }
        }

        self.inner.write(b" | ")?;

        for b in buf {
            if b.is_ascii_alphanumeric() || b.is_ascii_punctuation() {
                self.inner.write(&[*b])?;
            } else {
                self.inner.write(b".")?;
            }
        }

        if buf.len() < 16 {
            self.inner.write(&SPACE[buf.len()..16])?;
        }

        self.inner.write(b" |\n")?;
        self.pos += buf.len();
        Ok(buf.len())
    }
    fn flush(&mut self) -> io::Result<()> {
        self.inner.flush()
    }
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options] FILE", &program);
    print!("{}", opts.usage(&brief));
}

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optopt("o", "offset", "set offset", "OFFSET");
    opts.optflag("h", "help", "prints this help menu");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(e) => panic!(e.to_string())
    };

    if matches.opt_present("h") || matches.free.is_empty() {
        print_usage(&program, opts);
        return Ok(())
    }

    let filename = matches.free[0].clone();
    
    let offset = match matches.opt_str("o") {
        Some(v) => v.parse().unwrap(),
        None => 0
    };
    
    let mut f = File::open(filename)?;
    let metadata = f.metadata()?;

    let stdout = io::stdout();
    let lock = stdout.lock();

    let mut hw = HexWriter::new(lock, offset, 16, metadata.len() > u32::max_value() as u64);
    f.seek(SeekFrom::Start(offset as u64))?;

    let mut buf: Vec<u8> = vec![0; 16];
    while let Ok(len) = f.read(&mut buf) {
        if len == 0 {
            break;
        }
        hw.write(&buf[..len])?;
    }
    Ok(())
}
