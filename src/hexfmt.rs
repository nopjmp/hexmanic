use std::io::{self, Write};
const CHARS: &'static [u8] = b"0123456789abcdef";

// TODO: make the implementations a macro

pub trait WriteHex<W: Write> {
    fn write_hex(&self, w: &mut W) -> io::Result<usize>;
}

impl<W: Write> WriteHex<W> for u8 {
    fn write_hex(&self, w: &mut W) -> io::Result<usize> {
        w.write(&[CHARS[(self >> 4) as usize], CHARS[(self & 0xf) as usize]])
    }
}

impl<W: Write> WriteHex<W> for u32 {
    fn write_hex(&self, w: &mut W) -> io::Result<usize> {
        let mut array = [b'0'; 8];
        let mut value = *self;
        for n in (0..7).rev() {
            array[n] = CHARS[(value & 0xf) as usize];
            value >>= 4;
        }
        w.write(&array)
    }
}

impl<W: Write> WriteHex<W> for usize {
    fn write_hex(&self, w: &mut W) -> io::Result<usize> {
        let mut array = [b'0'; 16];
        let mut value = *self;
        for n in (0..15).rev() {
            array[n] = CHARS[(value & 0xf) as usize];
            value >>= 4;
        }
        w.write(&array)
    }
}
